#==============================================================================
# the_Cr0w's Base Script
#   Version: 1.00.00
#   Last Update: 2013-06-28
#   Website: http://www.the-crow.at
#   Author: the_Cr0w
#==============================================================================
# Licensed under Creative Commons BY-SA
#  http://creativecommons.org/licenses/by-sa/3.0/
#==============================================================================
# Some of my scripts require this script to work.
#==============================================================================

#==============================================================================
# DO NOT CHANGE ANYTHING BELOW THIS POINT
#==============================================================================

module Crow
  def self.require_script(name, require, version)
    if !$imported[require] || $imported[require] < version
      msg = "The script '%s' requires '%s' v%s or higher.\n"
      msg += "Go to http://www.the-crow.at to download it."
      
      name = name.to_s.gsub("_", " ").upcase
      require = require.to_s.gsub("_", " ").upcase
      
      msgbox(sprintf(msg, name, require, version))
      exit
    end
  end
end

$imported ||= {}
$imported[:crow_base_script] = 1.0000

#==============================================================================
# Game_Map
#==============================================================================
class Game_Map
  #--------------------------------------------------------------------------
  # [new] note
  #--------------------------------------------------------------------------
  def note
    @map ? @map.note : ""
  end
end # Game_Map
#==============================================================================
# Game_Enemy
#==============================================================================
class Game_Enemy
  #--------------------------------------------------------------------------
  # [new] note
  #--------------------------------------------------------------------------
  def note
    enemy ? enemy.note : ""
  end
end # Game_Enemy
#==============================================================================
# RPG::Troop::Page
#==============================================================================
class RPG::Troop::Page
  #--------------------------------------------------------------------------
  # [new] note
  #--------------------------------------------------------------------------
  def note
    return "" if !@list || @list.size <= 0
    comment_list = []
    
    @list.each do |item|
      next unless item && (item.code == 108 || item.code == 408)
      comment_list.push(item.parameters[0])
    end
    
    comment_list.join("\r\n")
  end
end # RPG::Troop::Page
#==============================================================================
# Game_Event
#==============================================================================
class Game_Event
  attr_reader :erased
  #--------------------------------------------------------------------------
  # [new] name
  #--------------------------------------------------------------------------
  def name
    @event.name
  end
  #--------------------------------------------------------------------------
  # [new] note
  #--------------------------------------------------------------------------
  def note
    return ""     if !@page || !@page.list || @page.list.size <= 0
    return @notes if @notes && @page.list == @note_page
    
    @note_page = @page.list.dup
    comment_list = []
    
    @page.list.each do |item|
      next unless item && (item.code == 108 || item.code == 408)
      comment_list.push(item.parameters[0])
    end
    
    @notes = comment_list.join("\r\n")
    @notes
  end  
end # Game_Event