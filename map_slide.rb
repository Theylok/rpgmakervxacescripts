#==============================================================================
# the_Cr0w's Map Slide Script
#   Version: 1.0000
#   Last Update: 2013-06-28
#   Website: http://www.the-crow.at
#   Author: the_Cr0w
#==============================================================================
# Licensed under Creative Commons BY-SA
#  http://creativecommons.org/licenses/by-sa/3.0/
#==============================================================================
# This script is based on OriginalWij's MapSlide Script for VX. It adds a
# Zelda-style map slide to the game when transfering to another map.
#==============================================================================
# Instructions
#------------------------------------------------------------------------------
# To use the map edge transfer place the following tag in the map's note box:
#
# <EdgeTransfer down_map_id left_map_id right_map_id up_map_id>
#
# *_map_id = id of the map the player should be transfered to
# use 0 (zero) to turn off edge transfer for this direction
#==============================================================================
# Requirements
#------------------------------------------------------------------------------
# Base Script 1.0000 or higher
#==============================================================================
# Updates
#------------------------------------------------------------------------------
# 2013-06-29 - Fixed a bug where the followers got placed on the opposite side
#              of the map
#==============================================================================

#==============================================================================
# Config
#==============================================================================
module Crow
  module MapSlide
    
    Speed = 4 # speed of the slide
    EdgeTransfer = true # Activate edge trasfer
    Exclude = [] # Exclude maps from edge transfer
    DisableSwitch = 0
    
  end
end

#==============================================================================
# DO NOT CHANGE ANYTHING BELOW THIS POINT
#==============================================================================

module Crow
  def self.require(name, require, version)
    if !$imported[:crow_base_script]
      msg = "The script '%s' requires 'Crow Base Script' v%s or higher.\n"
      msg += "Go to http://www.the-crow.at to download it."
      
      name = name.to_s.gsub("_", " ").upcase
      
      msgbox(sprintf(msg, name, version))
      exit
    else
      self.require_script(name, require, version)
    end
  end
end

$imported ||= {}
$imported[:crow_map_slide] = 1.0000
Crow.require(:crow_map_slide, :crow_base_script, 1.0000)

#==============================================================================
# Game_Temp
#==============================================================================
class Game_Temp
  #--------------------------------------------------------------------------
  # Public Instance Variables
  #--------------------------------------------------------------------------
  attr_accessor :scroll
  attr_accessor :no_trigger
  attr_accessor :slide_x
  attr_accessor :slide_y
  #--------------------------------------------------------------------------
  # [alias] initialize
  #--------------------------------------------------------------------------
  alias crow_mapslide_game_temp_initialize initialize
  def initialize
    crow_mapslide_game_temp_initialize
    
    @scroll = @no_trigger = false
    @slide_x = @slide_y = 0
  end # initialize
end # Game_Temp
#==============================================================================
# Game_Player
#==============================================================================
class Game_Player
  #--------------------------------------------------------------------------
  # [alias] check_event_trigger_here
  #--------------------------------------------------------------------------
  alias crow_mapslide_game_player_check_event_trigger_here check_event_trigger_here
  def check_event_trigger_here(triggers)
    if $game_temp.no_trigger
      $game_temp.no_trigger = false
      return false
    end
    crow_mapslide_game_player_check_event_trigger_here(triggers)
  end # check_event_trigger_here
  #--------------------------------------------------------------------------
  # [alias] move_by_input
  #--------------------------------------------------------------------------
  alias crow_mapslide_game_player_move_by_input move_by_input
  def move_by_input
    return if !movable? || $game_map.interpreter.running?
    
    if (self.x == 0 || self.x == ($game_map.width - 1) || self.y == 0 ||
        self.y == ($game_map.height - 1)) && Crow::MapSlide::EdgeTransfer &&
        !$game_temp.scroll
      dir = $game_player.direction
      map_id = [0] * 9
      
      $game_map.note.scan(/<EdgeTransfer (\d+) (\d+) (\d+) (\d+)>/i)
      map_id[2] = $1.to_i if self.y == ($game_map.height - 1)
      map_id[4] = $2.to_i if self.x == 0
      map_id[6] = $3.to_i if self.x == ($game_map.width - 1)
      map_id[8] = $4.to_i if self.y == 0
            
      if map_id[dir] == 0 || Crow::MapSlide::Exclude.include?(map_id[dir])
        crow_mapslide_game_player_move_by_input
        return
      end
      
      x = $game_player.x
      y = $game_player.y
      new_map = load_data(sprintf("Data/Map%03d.rvdata2", map_id[dir]))
      
      case dir
      when 2; y = 0
      when 4; x = (new_map.width - 1)
      when 6; x = 0
      when 8; y = (new_map.height - 1)
      end
      
      $game_player.reserve_transfer(map_id[dir], x, y, dir)
    else
      $game_temp.no_trigger = false
      crow_mapslide_game_player_move_by_input
    end
  end # move_by_input
  #--------------------------------------------------------------------------
  # [alias] passabel?
  #--------------------------------------------------------------------------
  alias crow_mapslide_game_player_passable? passable?
  def passabel?(*args)
    return true if $game_temp.scroll
    
    crow_mapslide_game_player_passable?(*args)
  end # passable?
  #--------------------------------------------------------------------------
  # [alias] debug_through?
  #--------------------------------------------------------------------------
  alias crow_mapslide_game_player_debug_through? debug_through?
  def debug_through?
    return true if $game_temp.scroll
    
    crow_mapslide_game_player_debug_through?
  end # debug_through?
  #--------------------------------------------------------------------------
  # [new] move2
  #--------------------------------------------------------------------------
  def move2(x, y)
    @x, @y = x, y
    @real_x, @real_y, @prelock_direction = @x, @y, 0
    straighten
    update_bush_depth
    
    center(x, y)
    make_encounter_count
    vehicle.refresh if vehicle
    @followers.synchronize(x, y, direction)
  end # move2
end # Game_Player
#==============================================================================
# Sprite_Character
#==============================================================================
class Sprite_Character
  #--------------------------------------------------------------------------
  # [alias] update
  #--------------------------------------------------------------------------
  alias crow_mapslide_sprite_character_update update
  def update
    crow_mapslide_sprite_character_update
    
    if $game_temp.scroll
      self.x = @character.screen_x + $game_temp.slide_x
      self.y = @character.screen_y + $game_temp.slide_y
      
      if @character.is_a?(Game_Player)
        self.x -= $game_temp.slide_x / 16
        self.y -= $game_temp.slide_y / 16
      end
    end
  end # update
end # Sprite_Character
#==============================================================================
# Spriteset_Map
#==============================================================================
class Spriteset_Map
  #--------------------------------------------------------------------------
  # Public Instance Variables
  #--------------------------------------------------------------------------
  attr_accessor :tilemap
  attr_reader :viewport1
  attr_reader :viewport2
  attr_reader :viewport3
end # Spriteset_Map
#==============================================================================
# Scene_Map
#==============================================================================
class Scene_Map
  #--------------------------------------------------------------------------
  # [alias] update_transfer_player
  #--------------------------------------------------------------------------
  alias crow_mapslide_scene_map_update_transfer_player update_transfer_player
  def update_transfer_player
    if $game_switches[Crow::MapSlide::DisableSwitch]
      crow_mapslide_scene_map_update_transfer_player
    else
      return unless $game_player.transfer?
      
      w = Graphics.width
      h = Graphics.height
      speed = Crow::MapSlide::Speed
            
      @spriteset.update
      $game_temp.scroll = true
      
      @hold = Sprite.new
      @hold.bitmap = Graphics.snap_to_bitmap
      @hold.z = 100
      
      if $game_player.vehicle
        $game_map.vehicles[$game_player.vehicle_type].transparent = true
      else
        $game_player.followers.synchronize($game_player.x, $game_player.y, $game_player.direction)
        $game_player.transparent = true
        $game_player.followers.visible = false
        $game_player.followers.refresh
      end
      
      @spriteset.dispose
      $game_player.perform_transfer
      $game_map.autoplay
      $game_map.update
      @spriteset = Spriteset_Map.new
      
      case $game_player.direction
      when 2
        @spriteset.tilemap.oy += h + ($game_map.height * 32 - h) * 2
        $game_temp.slide_x = 0
        $game_temp.slide_y = h
        
        for i in 0...h / speed
          @hold.oy += speed
          @spriteset.tilemap.oy += speed
          $game_temp.slide_y -= speed
          @spriteset.update_characters
          Graphics.update
        end
      when 4
        @spriteset.tilemap.ox -= w + ($game_map.width * 32 - w) * 2
        $game_temp.slide_x = -w
        $game_temp.slide_y = 0
        
        for i in 0...w / speed
          @hold.ox -= speed
          @spriteset.tilemap.ox -= speed
          $game_temp.slide_x += speed
          @spriteset.update_characters
          Graphics.update
        end
      when 6
        @spriteset.tilemap.ox += w + ($game_map.width * 32 - w) * 2
        $game_temp.slide_x = w
        $game_temp.slide_y = 0
        
        for i in 0...w / speed
          @hold.ox += speed
          @spriteset.tilemap.ox += speed
          $game_temp.slide_x -= speed
          @spriteset.update_characters
          Graphics.update
        end
      when 8
        @spriteset.tilemap.oy -= h + ($game_map.height * 32 - h) * 2
        $game_temp.slide_x = 0
        $game_temp.slide_y = -h
        
        for i in 0...h / speed
          @hold.oy -= speed
          @spriteset.tilemap.oy -= speed
          $game_temp.slide_y += speed
          @spriteset.update_characters
          Graphics.update
        end
      end
      
      @hold.bitmap.dispose
      @hold.dispose
      Input.update
            
      $game_temp.no_trigger = true
      
      player_x = $game_player.x
      player_y = $game_player.y
      player_dir = $game_player.direction
      
      case player_dir
      when 2
        $game_player.move2(player_x, player_y - 1)
      when 4
        $game_player.move2(player_x + 1, player_y)
      when 6
        $game_player.move2(player_x - 1, player_y)
      when 8
        $game_player.move2(player_x, player_y + 1)
      end
      
      if $game_player.vehicle
        $game_map.vehicles[$game_player.vehicle_type].transparent = false
      else
        $game_player.transparent = false
      end
      
      $game_player.move_straight(player_dir)
      
      player_x = $game_player.x
      player_y = $game_player.y
      player_dir = $game_player.direction
      
      $game_player.followers.synchronize(player_x, player_y, player_dir)
      
      $game_player.followers.visible = true
      $game_player.followers.refresh
      
      $game_temp.scroll = false
      
    end
  end # update_transfer_player
end # Scene_Map